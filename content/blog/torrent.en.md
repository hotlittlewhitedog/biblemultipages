+++
title = "Alternate Backups"
date = "2024-08-29"
category = "resources"
keywords = "['biblemulti.org', 'bible multi', bible kjv', 'kjv', 'bible app', 'bible apps', 'bible android', 'bible apk', 'bible ios', 'bible mac', 'bible linux', 'bible terminal', 'bible flatpak', 'bible snap', 'bible christian', 'christian', bible multi the light', 'biblemultithelight', bible multi the life', 'biblemultithelife', 'bible multi the son of man', 'biblemultithesonofman', 'sonofman', 'god', 'jesus', 'the life', 'the light', 'free', 'hlwd', 'torrent', 'evangelism', 'apocalypse', 'revelation', 'alternate backup', 'bible offline', 'bible torrent', 'alternate backup']" 
+++
  
:fire: To avoid future cenx0rChip, I took the decision to distribute the apps on several servers for direct download (centralized) and P2P (decentralized).  

If you download the apps via Bittorrent, please let the files in your download folder for several days to be a seeder. Let's think for 5 seconds about the people left behind during the Apocalypse! Personally, I use [qBittorrent](https://www.qbittorrent.org/download), it works very well. It has 2 speed modes (slow, fast) by clicking the icons in the status bar. You can also be a Superseeder. Until the files (apps) are in the P2P network they remain available for distribution :raised_hands: Nowadays a lot of people have solar panels and a decentralized network can't completely be destroyed without stopping internet ‼ Here are lists of good trackers to connect people; you might add to your existing torrents: [Github](https://github.com/ngosang/trackerslist)  

---

Always check the SHA256 hash of the corresponding file on BibleMulti.org before installing it on your device. If the site is not available, check a copy of the website in the Wayback Machine of Archive.org:   
	
On Linux:	sha256sum example.txt  
On Mac: 	shasum -a 256 example.txt  
On Windows: certutil -hashfile example.txt SHA256  

---

## __"The Light"__ 3.87 on 20240630:  
> APK for Android  

- __Signed by__ hotlittlewhitedog,  
It's the Google Play Store version.  
:hash: SHA256 of APK:  
187f0623ecc876cece5acfe0d43e3563dd34bc2542db48e392d14bd3a5776e6f&nbsp;&nbsp;  
[Torrent](https://archive.org/download/biblemultithelight_20240630_387_hotlittlewhitedog_sign/biblemultithelight_20240630_387_hotlittlewhitedog_sign_T2.torrent)  |  [Torrent](/biblemulti/blog/biblemultithelight_20240630_387_hotlittlewhitedog_sign_T2.torrent)  |  [Archive.org](https://archive.org/details/biblemultithelight_20240630_387_hotlittlewhitedog_sign)  |  [Google Drive](https://drive.google.com/drive/folders/1zFz3dxz3NslRgRyV7T1vyJyAMTBDE40O?usp=sharing)  |  [kDrive](https://kdrive.infomaniak.com/app/share/1237123/df4acc81-ff78-467a-93a3-5071f88d5d7c)  |  [pCloud](https://e.pcloud.link/publink/show?code=kZ8qdPZdfNaKcqD5qkrQveW6Qdo8pBNHPlk)

- __Signed by__ F-Droid,  
It's the F-Droid version.  
:hash: SHA256 of APK:  
7497283f08a369d5bb1a810c24b3cf918b873c153a886da7da7991655da9958b&nbsp;&nbsp;  
[Torrent](https://archive.org/download/biblemultithelight_20240630_387_fdroid_sign_202409/biblemultithelight_20240630_387_fdroid_sign_202409_archive.torrent)  |  [Torrent](/biblemulti/blog/biblemultithelight_20240630_387_fdroid_sign_202409_archive.torrent)  |  [Archive.org](https://archive.org/details/biblemultithelight_20240630_387_fdroid_sign_202409)  |  [Google Drive](https://drive.google.com/drive/folders/1zFz3dxz3NslRgRyV7T1vyJyAMTBDE40O?usp=sharing)  |  [kDrive](https://kdrive.infomaniak.com/app/share/1237123/df4acc81-ff78-467a-93a3-5071f88d5d7c)  |  [pCloud](https://e.pcloud.link/publink/show?code=kZ8qdPZdfNaKcqD5qkrQveW6Qdo8pBNHPlk)

  
## __"The Life"__ 1.27 on 20240705:  
> APK for Android  

- __Signed by__ hotlittlewhitedog,  
It's the Google Play Store version.  
:hash: SHA256 of APK:  
5f70151b11508c8069af40ba2a6809994a1de6807e9765731ba9c888a976f214&nbsp;&nbsp;
[Torrent](https://archive.org/download/biblemultithelife_20240705_127_hotlittlewhitedog_sign_20240831/biblemultithelife_20240705_127_hotlittlewhitedog_sign_20240831_T2.torrent)  |  [Torrent](/biblemulti/blog/biblemultithelife_20240705_127_hotlittlewhitedog_sign_20240831_T2.torrent)  |  [Archive.org](https://archive.org/details/biblemultithelife_20240705_127_hotlittlewhitedog_sign_20240831)  |  [Google Drive](https://drive.google.com/drive/folders/1zFz3dxz3NslRgRyV7T1vyJyAMTBDE40O?usp=sharing)  |  [kDrive](https://kdrive.infomaniak.com/app/share/1237123/df4acc81-ff78-467a-93a3-5071f88d5d7c)  |  [pCloud](https://e.pcloud.link/publink/show?code=kZ8qdPZdfNaKcqD5qkrQveW6Qdo8pBNHPlk)


- __Signed by__ F-Droid,  
It's the F-Droid version.  
:hash: SHA256 of APK:  
a021865ea97f3cae3547b8c6aed6ce58fe9ba3b50611c54692d054bcd527d80a&nbsp;&nbsp;  
[Torrent](https://archive.org/download/biblemultithelife_20240705_127_fdroid_sign_202409/biblemultithelife_20240705_127_fdroid_sign_202409_archive.torrent)  |  [Torrent](/biblemulti/blog/biblemultithelife_20240705_127_fdroid_sign_202409_archive.torrent)  |  [Archive.org](https://archive.org/details/biblemultithelife_20240705_127_fdroid_sign_202409)  |  [Google Drive](https://drive.google.com/drive/folders/1zFz3dxz3NslRgRyV7T1vyJyAMTBDE40O?usp=sharing)  |  [kDrive](https://kdrive.infomaniak.com/app/share/1237123/df4acc81-ff78-467a-93a3-5071f88d5d7c)  |  [pCloud](https://e.pcloud.link/publink/show?code=kZ8qdPZdfNaKcqD5qkrQveW6Qdo8pBNHPlk)

  
## __"The Sonofman"__ 3.3.0 on 20240824:  
> WHL for any  

- __Compiled by__ hotlittlewhitedog,  
It's the PyPI version.  
:hash: SHA256 of WHL:  
518e23f1deb1b99a54ba8f7e7fa8f0a1164005d8adcbc94bd839234c32eb3725&nbsp;&nbsp;  
[Torrent](https://archive.org/download/sonofman-3.3.0-py3-none-any/sonofman-3.3.0-py3-none-any_T2.torrent)  |  [Torrent](/biblemulti/blog/sonofman-3.3.0-py3-none-any_T2.torrent)  |  [Archive.org](https://archive.org/details/sonofman-3.3.0-py3-none-any)  |  [Google Drive](https://drive.google.com/drive/folders/1zFz3dxz3NslRgRyV7T1vyJyAMTBDE40O?usp=sharing)  |  [kDrive](https://kdrive.infomaniak.com/app/share/1237123/df4acc81-ff78-467a-93a3-5071f88d5d7c)  |  [pCloud](https://e.pcloud.link/publink/show?code=kZ8qdPZdfNaKcqD5qkrQveW6Qdo8pBNHPlk)

    
** All The Glory To God :heart:
