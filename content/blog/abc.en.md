+++
title = "How to be saved?"
date = "2022-08-10"
category = "resources"
+++

{{< youtube 03mdmJshse4 >}}

#
# The Apostle Paul tells us what the gospel is

> **1Corinthians 15:1-4**: Moreover, brethren, I declare to you the gospel which I preached to you, which also you received and in which you 
stand, by which also you are saved, if you hold fast that word which I preached to you, unless you believed in vain. For I delivered to you 
first of all that which I also received: that Christ died for our sins according to the Scriptures, and that He was buried, and that He rose 
again the third day according to the Scriptures.

#
# As simple as an ABC
  
**A) ADMIT THAT YOU'RE A SINNER.**
  
  This is where that godly sorrow leads to genuine repentance for sinning against the righteous God and there is a change of heart, we change 
our mind and God changes our hearts and regenates us from the inside out.  

---
  
**B) BELIEVE IN YOUR HEART THAT JESUS CHRIST DIED FOR YOUR SINS, WAS BURIED, AND THAT GOD RAISED JESUS FROM THE DEAD.**
  
  This is trusting with all of your heart that Jesus Christ is who he said he was and he died for us for our sins.  

---
  
**C) CALL UPON THE NAME OF THE LORD.**
  
  This is trusting with all of your heart that Jesus Christ is who he said he was. Every single person who ever lived since Adam will bend 
their knee and confess with their mouth that Jesus Christ is Lord, the Lord of Lords and the King of Kings.  
  

