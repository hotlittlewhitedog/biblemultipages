+++
title = "¿Cómo ser salvo?"
date = "2022-08-10"
category = "resources"
+++

{{< youtube d44YwG_3SUA >}}

#
# El Apóstol Pablo nos dice qué es el evangelio

> **1Corintios 15:1-4**: Además, hermanos, os declaro el evangelio que os prediqué y que recibisteis y en el cual también estáis firmes; por el cual también sois salvos, si lo retenéis como yo os lo he predicado. De otro modo, creísteis en vano. Porque en primer lugar os he enseñado lo que también recibí: que Cristo murió por nuestros pecados, conforme a las Escrituras; que fue sepultado y que resucitó al tercer día, conforme a las Escrituras;

#
# Tan simple como un ABC
  
**A) ADMITA QUE USTED ES PECADOR.**
  
  Esto sucede cuando siente un pesar que viene de Dios y lo lleva a un arrepentimiento genuino por haber pecado contra un Dios justo; se produce un cambio en el corazón, cambiamos nuestra mente y Dios cambia nuestro corazón regenerándonos desde adentro hacia afuera.  

---
  
**B) CREE EN JESUCRISTO ES EL SEÑOR Y CREE CON TU CORAZÓN QUE JESUCRISTO MURIÓ POR SUS PECADOS, FUE SEPULTADO Y QUE DIOS LO LEVANTÓ DE LOS MUERTOS.**
  
  Es creer con todo tu corazón que Jesucristo es lo que dijo que era y que murió por nosotros por nuestros pecados.  

---
  
**C) CONFIESE EL NOMBRE DEL SEÑOR.**
  
  Es creer con todo tu corazón que Jesucristo es lo que dijo que era. Cada persona que ha vivido desde Adán doblará la rodilla y confesará con su boca que Jesucristo es el Señor, el Señor de los Señores y el Rey de los Reyes.  
  

