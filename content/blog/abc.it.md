+++
title = "Come essere salvato?"
date = "2022-08-10"
category = "resources"
+++

{{< youtube iRxQ_6_3fkQ >}}

#
# L'apostolo Paolo ci dice cos'è il vangelo

> **1Corinzi 15:1-4**: Ora, fratelli, vi dichiaro l'evangelo che vi ho annunziato, e che voi avete ricevuto e nel quale state saldi, e mediante il quale siete salvati, se ritenete fermamente quella parola che vi ho annunziato, a meno che non abbiate creduto invano. Infatti vi ho prima di tutto trasmesso ciò che ho anch'io ricevuto, e cioè che Cristo è morto per i nostri peccati secondo le Scritture, che fu sepolto e risuscitò a il terzo giorno secondo le Scritture,

#
# Semplice come un ABC
  
**A) AMMETTI DI ESSERE UN PECCATORE.**
  
  È qui che quella tristezza pia conduce al vero pentimento per aver peccato contro un dio giusto. C’è un cambiamento di cuore e di spirito. Dio ci cambia il cuore e ci rigenera dall’interno.  

---
  
**B) BEN CREDI CHE GESÙ CRISTO SIA IL SIGNORE E BEN CREDI NEL TUO CUORE CHE GESÙ CRISTO È MORTO PER I TUOI PECCATI, È STATO SEPOLTO, E CHE DIO HA RISUSCITATO GESÙ DALLA MORTE.**
  
  È credere con tutto il cuore che Gesù Cristo è ciò che ha detto di essere e che è morto per noi per i nostri peccati.  

---
  
**C) CHIAMI ED INVOCHI IL NOME DEL SIGNORE.**
  
  È credere con tutto il cuore che Gesù Cristo è ciò che ha detto di essere. Ogni persona che ha vissuto dopo Adamo piegherà il ginocchio e confesserà con la sua bocca che Gesù Cristo è Signore, il Signore dei Signori e il Re dei Re.  
  

