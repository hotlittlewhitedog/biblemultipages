+++
title = "Comment être sauvé?"
date = "2022-08-10"
category = "resources"
+++

{{< youtube O5_StJOoBaI >}}

#
# L'apôtre Paul nous dit ce qu'est l'évangile

> **1Corinthiens 15:1-4**: Je vous rappelle, frères, l'Évangile que je vous ai annoncé, que vous avez reçu, dans lequel vous avez persévéré, et par lequel vous êtes sauvés, si vous le retenez tel que je vous l'ai annoncé; autrement, vous auriez cru en vain. Je vous ai enseigné avant tout, comme je l'avais aussi reçu, que Christ est mort pour nos péchés, selon les Écritures; qu'il a été enseveli, et qu'il est ressuscité le troisième jour, selon les Écritures;

#
# Simple comme un ABC
  
**A) ADMET QUE TU ES UN PÉCHEUR.**
  
  C'est là que cette tristesse selon Dieu conduit à une véritable repentance pour avoir péché contre le Dieu juste et il y a un changement de coeur, nous changeons d'avis et Dieu change nos coeurs et nous régénère de l'intérieur.  

---
  
**B) BIEN CROIRE EN JÉSUS CHRIST, QU'IL EST LE SEIGNEUR ET CROYEZ DANS VOTRE COEUR QUE JÉSUS CHRIST EST MORT POUR VOS PÉCHÉS, A ÉTÉ ENTERRÉ ET QUE DIEU A RESSUSCITÉ JÉSUS D'ENTRE LES MORTS.**
  
  C'est croire de tout votre coeur que Jésus-Christ est ce qu'il a dit qu'il était et qu'il est mort pour nous pour nos péchés.  

---
  
**C) CONFESSE À JÉSUS CHRIST QUE TU CROIS EN LUI.**
  
  C'est croire de tout votre coeur que Jésus-Christ est ce qu'il a dit qu'il était. Chaque personne qui a vécu depuis Adam pliera le genou et confessera de sa bouche que Jésus-Christ est Seigneur, le Seigneur des Seigneurs et le Roi des Rois.  
    

