+++
title = "Ebook: The Rapture Survival Guide"
date = "2022-07-15"
category = "resources"
+++

Be vigilant, always check in the Bible.
  
! I'm not the creator of this ebook.  

Feel free to download this Rapture guide for people who will be left behind.  

Think to copy it, share it, print it, send it, as soon as possible.  

If you missed the rapture and want to know what happens, this ebook is very useful and will provide a lot of info.  

You can jump to specific sections.  

Of course, read the Bible and let the holy spirit works in you and guides you.  

#
# To make short
Accept Jesus Christ as Lord and Saviour, only him can save you/us from eternity in Hell (lake of fire). Believe that you/we are all sinners and need a saviour. 
Believe that Jesus, the son of God came on Earth and gave his life, his precious blood for people believing in him. So if you believe in him, repent (yes, repent 
with your heart, it's absolutely necessary, don't repent to men or saints or angels or Mary but God the father in the name of Jesus). Believe that Jesus died on 
the cross (yes, he came to die for us, like the perfect lamb of God) and really died. He was resurrected 3 days later by his father (our father). Confess Jesus in 
your prayer to God and God will give you a new heart, you will be born again, a new creature. Stay away from sins as much as you can and walk each days in the 
Spirit. Make your best to seek the Lord, pray, meditate the word of God, talk to him (be careful that the devil will also talk to you, have discernment), be holy, 
be sanctified and continue to share the gospel, the word of God, the good news of salvation.  

! Don't take the mark of the beast (mark on the right hand or the forehead). It's better to die for Jesus Christ than take the mark of the beast (Devil). 
 
People who will take the mark of the beast will go to Hell and will be tormented days and nights.

#
# Several URLs
(where to get the ebook, some links might be broken)

## Backup
(could not be the last version but there are less trackers)
- [Backup version](https://gitlab.com/hotlittlewhitedog/BibleTheLife/-/tree/master/ebooks)

## From original website
(take the latest TRSG file)
- [Last version](https://fluidicice.com/downloads)

## Original website
- [Home](https://fluidicice.com)

## Alternate backups
- [Google Drive](https://www.drive.google.com/drive/folders/1KVzoaRSuHUQl8Zj-mXPYIJ0iEi14PZc3)
- [Box](https://app.box.com/s/jplaw5euljmybbzqk02m0o3g4y2hcm84)
- [Dropbox](https://www.dropbox.com/sh/ljd2sitt9tztzm1z/AADNRiWtvTr1zZ1nBVQbkkI3a)
- [OneDrive (1)](https://1drv.ms/u/s!AuxDPqkBHS-3zQMa-66lGrqgklFj?e=RPQdt5)
- [OneDrive (2)](https://1drv.ms/u/s!AkxVecxqAulvgSiAaU9QvsVCtzKU?e=JVgbTo)
- [MediaFire](https://www.mediafire.com/folder/bp0tscrrc6fyj/Creative_Content)
