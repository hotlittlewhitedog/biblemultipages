+++
title = "Ubuntu Christian Edition"
date = "2022-07-02"
category = "resources"
+++

Ubuntu Christian Edition (UbuntuCE) is a free, open source operating system geared towards Christians. It is based on ["Ubuntu 22.04 LTS"](https://ubuntu.com/download/desktop).  

Ubuntu is a complete Linux-based operating system, freely available with both community and professional support.  

The goal of UbuntuCE is to bring the power and security of Ubuntu to Christians.
  
["UbuntuCE"](https://ubuntuce.com)
